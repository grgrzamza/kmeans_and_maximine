from random import randint
from sys import argv

def generate_vectors(count, high_bound):    
    result = list()
    for _ in range(count):
        result.append(tuple(randint(0, high_bound) for _ in range(2)))
    return result
