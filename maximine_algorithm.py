from kmeans_algorithm import distance, correlate_vectors
from random import randrange
from collections import defaultdict
from statistics import mean
from itertools import combinations

def average_centers_distance(areas):
    centers = list(areas.keys())
    if len(centers) > 1:
        return mean((distance(x, y) for x, y in combinations(centers, 2)))
    else:
        return 0

def get_new_center_index(vectors, areas):
    result = None
    max_in_areas = []
    for center, area in areas.items():
        max_in_areas.append(max(((i, distance(center, vectors[i])) for i in area), key=(lambda x: x[1])))
    total_max = max(max_in_areas, key=(lambda x: x[1]))
    if total_max[1] > (average_centers_distance(areas) / 2):
        result = total_max[0]
    return result

def maximine_algorithm(vectors):
    centers = []
    areas = None
    new_center_index = randrange(len(vectors))
    while new_center_index is not None:
        centers.append(vectors[new_center_index])
        areas = correlate_vectors(vectors, centers)
        new_center_index = get_new_center_index(vectors, areas)
    return areas