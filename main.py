from graphic import display_result
from sys import argv
from kmeans_algorithm import kmeans_algorithm
from maximine_algorithm import maximine_algorithm
from vectors_generator import generate_vectors
import os

def main():
    vectors_count = int(argv[1])
    use_kmeans = len(argv) == 3
    if use_kmeans:
        areas_count = int(argv[2])
    vectors = generate_vectors(vectors_count, 1000)
    if vectors:
        if use_kmeans:
            areas = kmeans_algorithm(vectors, areas_count, 30)
        else:
            areas = maximine_algorithm(vectors)
        display_result(vectors, areas, use_kmeans)

main()