from random import sample
from math import sqrt
from collections import defaultdict

def distance(first_vector, second_vector):
    return sqrt(sum(map(lambda a: a*a, (x-y for x, y in zip(first_vector, second_vector)))))

def correlate_vectors(vectors, centers):
    result = defaultdict(list)
    for vector_index, vector in enumerate(vectors):
        center = min(centers, key=lambda x: distance(vector, x))
        result[center].append(vector_index)
    return result

def recalculate_centers(vectors, vector_indexes):
    dimensions = len(vectors[0])
    result = [None] * dimensions
    for dimension in range(dimensions):
        result[dimension] = sum(vectors[i][dimension] for i in vector_indexes) / len(vector_indexes)
    return tuple(result)

def kmeans_algorithm(vectors, areas_count, max_iterations):
    result = None
    centers = sample(vectors, areas_count)
    for _ in range(max_iterations):
        result = correlate_vectors(vectors, centers)
        centers = set(map(lambda x: recalculate_centers(vectors, x), result.values()))
        previous_centers = set(result.keys())
        if previous_centers == centers:
            break
    return result